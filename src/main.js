import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import ViewUI from "view-design";
import "view-design/dist/styles/iview.css";
import axios from "axios";
import VueAxios from "vue-axios";
import { hasPermission } from "@/utils/permission.js"; // 按钮权限指令
import astgo from "@/assets/js/astgo.js";

import VueAMap from "vue-amap";
// 初始化vue-amap
Vue.use(VueAMap);
VueAMap.initAMapApiLoader({
  // 高德的key
  key: "0e445dca28965d5f7eb3f9d80de49039",
  // 插件集合
  plugin: [
    "AMap.Autocomplete",
    "AMap.PlaceSearch",
    "AMap.Scale",
    "AMap.OverView",
    "AMap.ToolBar",
    "AMap.MapType",
    "AMap.PolyEditor",
    "AMap.CircleEditor",
    "AMap.Geocoder"
  ],
  // 高德 sdk 版本，默认为 1.4.4
  v: "1.4.4"
});

axios.defaults.timeout = 0;
// axios.defaults.baseURL = "http://deviceapi.ys-n.com/admin/";

// axios.defaults.baseURL = "http://api.fv.smartkiosk.ijooz.sg/index.php/admin/";
// axios.defaults.baseMURL = "http://api.fv.smartkiosk.ijooz.sg/index.php/api/";

axios.defaults.baseURL =
  "https://api.green.smartkiosk.test.service.ijooz.com/index.php/admin/";
axios.defaults.baseMURL =
  "https://api.green.smartkiosk.test.service.ijooz.com/index.php/api/";

Vue.prototype.astgo = astgo;

Vue.prototype.Pn = "ys_prj_";

Vue.prototype.$testEmail = function(email) {
  var reg = /^([a-zA-Z]|[0-9])(\w|\-)+@[a-zA-Z0-9]+\.([a-zA-Z]{2,4})$/;
  if (reg.test(email)) {
    return true;
  } else {
    return false;
  }
};

Vue.prototype.$preview = function(img) {
  ViewUI.Modal.info({
    title: "预览",
    render: h => {
      if (Array.isArray(img)) {
        // 数组
        const newArr = [];
        img.forEach((element, index) => {
          newArr.push(
            h("img", {
              attrs: {
                src: element
              },
              style: {
                width: "100%",
                marginBottom: "10px"
              }
            })
          );
        });

        return h("div", newArr);
      } else {
        // 字符串
        return h("img", {
          attrs: {
            src: img
          },
          style: {
            width: "100%"
          }
        });
      }
    }
  });
};

Vue.config.productionTip = false;
Vue.use(hasPermission);
Vue.use(VueAxios, axios);
Vue.use(ViewUI);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
