import Vue from 'vue'
import Vuex from 'vuex'
import persistedState from 'vuex-persistedstate'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    width: 0,
    height: 0,
    refresh: true,
    menuList: [
      {
        name: '首页',
        icon: 'md-home',
        path: '/'
      },
      {
        name: '账号管理',
        icon: 'logo-buffer',
        path: '/account/index',
        id: 8
      },
      {
        name: '权限设置',
        icon: 'md-share-alt',
        path: '/system/powerset',
        id: 1
      },
      {
        name: '机器组管理',
        icon: 'ios-albums',
        path: '/team/index',
        id: 14
      },
      {
        name: '操作员管理',
        icon: 'ios-body',
        path: '/operator/index',
        id: 19
      },
      {
        name: '机器管理',
        icon: 'ios-browsers',
        path: '/equipment/index',
        id: 24
      },
      {
        name: '普通订单',
        icon: 'ios-browsers',
        path: '/order/index',
        id: 36
      },
      {
        name: '异常订单',
        icon: 'ios-browsers',
        path: '/order/fail',
        id: 38
      },
      {
        name: '统计表下载',
        icon: 'ios-browsers',
        path: '/tableDownload/index',
        id: 40
      },
      {
        name: '商品管理',
        icon: 'ios-browsers',
        path: '/goods/index',
        id: 45
      },
      {
        name: '门店管理',
        icon: 'ios-browsers',
        path: '/shop/index',
        id: 52
      },
      {
        name: '库存管理',
        icon: 'ios-browsers',
        path: '/stock/index',
        id: 59
      },
      {
        name: '上下货查询',
        icon: 'ios-browsers',
        path: '/stock/updownsearch',
        id: 65
      }

      // {
      // 	name:'订单管理',
      // 	icon:'md-apps',
      // 	path:'/order',
      // 	id:36,
      // 	children:[
      // 	  {
      // 	    name:'',
      // 	    path:'/order/index',
      // 		id:,
      // 	  },
      // 	  {
      // 	    name:'',
      // 	    path:'/order/fail',
      // 		id:,
      // 	    // path:'/order/abnormalDetail',
      // 	  },
      // 	//   {
      // 	//     name:'审核订单',
      // 	//     // path:'/order/fail',
      // 	//     // path:'/order/abnormalDetail',
      // 	//     path:'/order/abnormalList',
      // 	//   }
      // 	],
      // },
      // {
      // 	name:'manager机器组管理',
      // 	icon:'ios-albums',
      // 	path:'/manager/tuandui'
      // },
      // {
      // 	name:'manager操作员管理',
      // 	icon:'ios-body',
      // 	path:'/manager/czy'
      // },
      // {
      // 	name:'manager机器管理',
      // 	icon:'ios-browsers',
      // 	path:'/manager/shebei'	,
      // },
      // {
      // 	name:'operator机器组管理',
      // 	icon:'ios-albums',
      // 	path:'/operator/tuandui'
      // },
    ],
    Breadcrumb: ['1', '2', '3', '4'] // 1增2删3改4查
  },
  mutations: {
    editwidth (state, data) {
      state.width = data
    },
    editheight (state, data) {
      state.height = data
    },
    isMobile (state, data) {
      state.isMobile = data
    },
    setBreadcrumb (state, data) {
      state.Breadcrumb = data
    },
    setMenuList (state, data) {
      state.menuList = data
    },
    setRefresh (state, data) {
      state.refresh = data
    },
    setMenu (state, data) {
      if (data) {
        state.menuList = [{
          name: '首页',
          icon: 'md-home',
          path: '/'
        },
        {
          name: '用户管理',
          icon: 'md-home',
          path: '/user/index'
        },
        {
          name: '权限管理',
          icon: 'md-share-alt',
          path: '/jurisdiction',
          children: [{
            name: '权限列表',
            path: '/jurisdiction/index'
          },
          {
            name: '角色列表',
            path: '/jurisdiction/role'
          }

          ]
        }]
      } else {
        state.menuList = [{
          name: '首页',
          icon: 'md-home',
          path: '/'
        },
        {
          name: '用户管理',
          icon: 'md-home',
          path: '/user/index'
        }]
      }
    }
  },
  actions: {},
  modules: {}
  // plugins: [persistedState()]
})
