export default [
  // {
  // 	name:'首页',
  // 	icon:'md-home',
  // 	path:'/',
  // 	id:0
  // },

  // id： 1权限设置 8账号管理  14机器组管理  19操作员管理  24机器管理

  {
    name: '用户管理',
    icon: 'md-home',
    path: '/user/',
    id: 8,
    children: [
      {
        name: '用户列表',
        path: '/user/index',
        id: 8
      },
      {
        name: '添加管理员',
        path: '/user/add',
        id: 8
      }

    ]
  },

  {
    name: '账号管理',
    icon: 'md-home',
    path: '/account/',
    id: 8,
    children: [
      {
        name: '账号列表',
        path: '/account/index',
        id: 8
      },
      {
        name: '机器组',
        path: '/account/team',
        id: 8
      },
      {
        name: '查看机器组',
        path: '/account/teamLook',
        id: 8
      },
      {
        name: '操作员列表',
        path: '/account/opearator',
        id: 8
      }
    ]
  },
  {
    name: '机器组管理',
    icon: 'md-home',
    path: '/team/',
    id: 14,
    children: [
      {
        name: '机器组列表',
        path: '/team/index',
        id: 14
      }

    ]
  },
  {
    name: '操作员管理',
    icon: 'md-home',
    path: '/operator/',
    id: 19,
    children: [
      {
        name: '操作员列表',
        path: '/operator/index',
        id: 19
      }

    ]
  },
  {
    name: '机器管理',
    icon: 'md-home',
    path: '/equipment/',
    id: 24,
    children: [
      {
        name: '机器列表',
        path: '/equipment/index',
        id: 24
      }

    ]
  },
  {
    name: '普通订单',
    icon: 'md-apps',
    path: '/order/',
    id: 36,
    children: [
      {
        name: '异常订单',
        path: '/order/index',
        id: 38
      },
      {
        name: '异常订单审核',
        path: '/order/detail',
        id: 38
      }

    ]
  },
  {
    name: '异常订单',
    icon: 'md-apps',
    path: '/order/',
    id: 38,
    children: [
      {
        name: '异常列表',
        path: '/order/fail',
        id: 38
      },
      {
        name: '异常订单审核',
        path: '/order/abnormalDetail',
        id: 38
      }

    ]
  },
  {
    name: '统计表下载',
    icon: 'md-apps',
    path: '/tableDownload/',
    id: 14,
    children: [
      {
        name: '异常列表',
        path: '/tableDownload/index',
        id: 14
      }

    ]
  },
  {
    name: '商品管理',
    icon: 'md-apps',
    path: '/goods/',
    id: 14,
    children: [
      {
        name: '商品管理',
        path: '/goods/index',
        id: 14
      }

    ]
  },
  {
    name: '门店管理',
    icon: 'md-apps',
    path: '/shop/',
    id: 14,
    children: [
      {
        name: '门店管理',
        path: '/shop/index',
        id: 14
      }

    ]
  },
  {
    name: '库存管理',
    icon: 'md-apps',
    path: '/stock/',
    id: 14,
    children: [
      {
        name: '库存管理',
        path: '/stock/index',
        id: 14
      }

    ]
  },
  {
    name: '上下货查询',
    icon: 'md-apps',
    path: '/stock/',
    id: 14,
    children: [
      {
        name: '上下货查询',
        path: '/stock/updownsearch',
        id: 14
      }

    ]
  },
  // {
  // 	name: '订单管理',
  // 	icon: 'md-apps',
  // 	path: '/order/',
  // 	id: 36,
  // 	children: [
  // 		{
  // 			name: '普通订单',
  // 			path: '/order/index',
  // 			id: 36,
  // 		},
  // 		{
  // 			name: '订单详情',
  // 			path: '/order/detail',
  // 			id: 36,
  // 		},
  // 		{
  // 			name: '异常订单',
  // 			path: '/order/fail',
  // 			id: 38,
  // 		},
  // 		{
  // 			name: '异常订单审核',
  // 			path: '/order/abnormalDetail',
  // 			id: 38,
  // 		},

  // 	]
  // },

  {
    name: '系统设置',
    icon: 'md-share-alt',
    path: '/system',
    id: 1,
    children: [
      {
        name: '权限设置',
        path: '/system/allpower',
        id: 1
      },
      {
        name: '权限设置',
        path: '/system/powerset',
        id: 1
      },
      {
        name: '密码修改',
        path: '/system/password',
        id: '0'
      }

    ]
  }

]
