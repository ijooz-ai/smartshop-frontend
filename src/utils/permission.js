import store from '../store'
import Vue from 'vue'
import astgo from '@/assets/js/astgo.js'
export const hasPermission = {
    install (Vue) {
        Vue.directive('hasPermission', {
            bind (el, binding, vnode) {
                // const permissionsNameList = JSON.parse(localStorage.getItem("permissionsList"));
                let  permissionsNameList = JSON.parse(astgo.getItem("ys_prj_info_role")) ; 
                // let  permissionsNameList = []; 
				
			
				
                let permissions = permissionsNameList
                let value = binding.value
                let flag = true
                for (let v of value) {
                    if (!permissions.includes(v)) {
                        flag = false
                    }
                }
                if (!flag) {
                    if (!el.parentNode) {
                        el.style.display = 'none'
                    } else {
                        el.parentNode.removeChild(el)
                    }
                }
            }
        })
    }
}