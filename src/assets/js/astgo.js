import CryptoJS from 'crypto-js';
import { Spin } from 'view-design';
const astgo = {
    uploadfileing:"",
    key:CryptoJS.enc.Utf8.parse("1234123412ABCDEF"),
    iv:CryptoJS.enc.Utf8.parse("ABCDEF1234123412"),
    encrypt(word){  //加密
        let srcs = CryptoJS.enc.Utf8.parse(word);
        let encrypted = CryptoJS.AES.encrypt(srcs, this.key, { iv: this.iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
        return encrypted.ciphertext.toString().toUpperCase();
    },
    decrypt(word){       //解密
        let encryptedHexStr = CryptoJS.enc.Hex.parse(word);
        let srcs = CryptoJS.enc.Base64.stringify(encryptedHexStr);
        let decrypt = CryptoJS.AES.decrypt(srcs, this.key, { iv: this.iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
        let decryptedStr = decrypt.toString(CryptoJS.enc.Utf8);
        return decryptedStr.toString();
    },
    setItem(key,value){
        var string = this.encrypt(value);
        // localStorage.setItem(key,string);
        sessionStorage.setItem(key,string);
        return string;
    },
    getItem(key){
			
        // var value = localStorage.getItem(key);
        var value = sessionStorage.getItem(key);
        
        return value ? this.decrypt(value) : false;
    },
    s(key,value=''){       //s('key'); 读取  s('key','value') 设置 s('key',null)
        if(value == null){
            // localStorage.removeItem(key);
            sessionStorage.removeItem(key);
        }else if(value){
            var string = this.encrypt(value);
            // localStorage.setItem(key,string);
            sessionStorage.setItem(key,string);
            return string;
        }else{
            // var g = localStorage.getItem(key);
            var g =  sessionStorage.getItem(key);
            return g ? this.decrypt(g) : false;
        }
    },
    removeItem(key){
        // localStorage.removeItem(key);
        sessionStorage.removeItem(key);
    },
    randomString(len) {
    　　var len = len || 32;
    　　var $chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678';    /****默认去掉了容易混淆的字符oOLl,9gq,Vv,Uu,I1****/
    　　var maxPos = $chars.length;
    　　var pwd = '';
    　　for (var i = 0; i < len; i++) {
    　　　　pwd += $chars.charAt(Math.floor(Math.random() * maxPos));
    　　}
    　　return pwd;
    },
    // setItem(key, value) {
    //   return sessionStorage.setItem(key, value);
    // },
    // getItem(key) {
    //   return sessionStorage.getItem(key);
    // },
    // removeItem(key) {
    //   return sessionStorage.removeItem(key);
    // },
    d() {
      var d = new Date();
      var H = d.getHours();
      var M = d.getMinutes();
      var S = d.getSeconds();
      if (H < 10) {
        H = "0" + H;
      } else if (M < 10) {
        M = "0" + M;
      } else if (S < 10) {
        S = "0" + S;
      }
      var date = H + ":" + M + ":" + S;
      return date;
    },
    dateFormat(fmt, date) {     // 封装搜索条件日期
      let ret;
      let opt = {
        "Y+": date.getFullYear().toString(), // 年
        "m+": (date.getMonth() + 1).toString(), // 月
        "d+": date.getDate().toString(), // 日
        "H+": date.getHours().toString(), // 时
        "M+": date.getMinutes().toString(), // 分
        "S+": date.getSeconds().toString() // 秒
        // 有其他格式化字符需求可以继续添加，必须转化成字符串
      };
      for (let k in opt) {
        ret = new RegExp("(" + k + ")").exec(fmt);
        if (ret) {
          fmt = fmt.replace(ret[1], (ret[1].length == 1) ? (opt[k]) : (opt[k].padStart(ret[1].length, "0")));
        }
      }
      return fmt;
    },
    str_replace (string) {
      if(string){
        return string.replace('\n','<br />');
      }else{
        return;
      }
    },
    globalLoading (a) {   // loading
      var percent = parseInt(a.percent);

      Spin.percent = percent;

     var spinloading = Spin.show({
        render: (h) => {
          return h('div', [
            h('Icon', {
              'class': 'demo-spin-icon-load',
              props: {
                type: 'ios-loading',
                size: 18
              }
            }),
            h('div','正在上传中...'+self.percent+'%')
          ])
        },
      });
    },
	  Menu(menu, data) {  //menu 全部的侧边栏    data当前用户拥有权限的侧边栏
	    for (let i = menu.length - 1; i >= 0; i--) {
	      if (menu[i].list) {
	        for (let k = menu[i].list.length - 1; k >= 0; k--) {
	          for (let l in data) {
	            if (data[l] == menu[i].list[k].url) {
	              break;
	            } else if (l == data.length - 1) {
	              menu[i].list.splice(k, 1);
	            }
	          }
	        }
	        if (menu[i].list.length == 0) {
	          menu.splice(i, 1);
	        }
	      } else {
	        for (let j in data) {
	          if (data[j] == menu[i].url) {
	            break;
	          } else if (j == data.length - 1) {
	            menu.splice(i, 1);
	          }
	        }
	      }
	    }
	  },
  };
export default astgo;