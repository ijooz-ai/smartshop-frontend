module.exports = {
  publicPath: "/admin/",
  lintOnSave: false,
  productionSourceMap: false,
  devServer: {
    open: true 
  }
}